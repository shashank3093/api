package com.hospital.patientrecords.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hospital.patientrecords.model.Patient;
import com.hospital.patientrecords.model.PatientRecordResponse;
import com.hospital.patientrecords.service.PatientService;

@RestController
@RequestMapping("/hospital/")
public class PatientRecordController {

	Logger log = LoggerFactory.getLogger(PatientRecordController.class);

	@Autowired
	PatientService patientService;

	@GetMapping("/patients/{id}")
	public ResponseEntity<PatientRecordResponse> getPatientRecord(@PathVariable("id") int patientId) throws Exception {
		Patient patientData = new Patient();
		patientData.setId(patientId);
		Optional<Patient> patientRecord = patientService.getPatientRecords(patientData);
		PatientRecordResponse patientdetails = new PatientRecordResponse();
		patientdetails.setPatientRecords(List.of(patientRecord.get()));
		return new ResponseEntity<>(patientdetails, HttpStatus.OK);

	}

	@GetMapping("/patients")
	public ResponseEntity<PatientRecordResponse> getAllPatients() throws Exception {

		List<Patient> patientDetails = patientService.getAllPatients();
		PatientRecordResponse patientdetails = new PatientRecordResponse();
		patientdetails.setPatientRecords(patientDetails);
		return new ResponseEntity<>(patientdetails, HttpStatus.OK);

	}

	@PostMapping("/addPatient")
	public ResponseEntity<PatientRecordResponse> addPatientRecord(@Valid @RequestBody Patient patient) {
		PatientRecordResponse patientdetails = new PatientRecordResponse();
		Patient persistedPatientRecord = patientService.addPatientRecord(patient);
		patientdetails.setPatientRecords(List.of(persistedPatientRecord));
		return new ResponseEntity<>(patientdetails, HttpStatus.OK);
	}

	@PutMapping("/updatePatient")
	public ResponseEntity<PatientRecordResponse> updatePatientRecord(@Valid @RequestBody Patient patient) {
		PatientRecordResponse patientdetails = new PatientRecordResponse();
		Patient persistedPatientRecord = patientService.updatePatientRecord(patient);
		patientdetails.setPatientRecords(List.of(persistedPatientRecord));
		return new ResponseEntity<>(patientdetails, HttpStatus.OK);
	}

	@DeleteMapping("/deletePatient/{id}")
	public ResponseEntity<PatientRecordResponse> deleteTutorial(@PathVariable("id") long patientId) {
		PatientRecordResponse patientdetails = new PatientRecordResponse();
		Patient patientData = new Patient();
		patientData.setId(patientId);
		patientService.deletePatientRecord(patientData);
		return new ResponseEntity<>(patientdetails, HttpStatus.OK);
	}

}
