package com.hospital.patientrecords.util;

public final class PatientConstants {

	public static final String I_PATIENT_RECORD_MSG = "Invalid Patient. Requested record Not Found";

	public static final String M_PATIENT_RECORD = "M_PATIENT_RECORD";

	public static final String M_PATIENT_RECORD_MSG = "Missing All Patient Records";

	public static final String REGULAR = "REGULAR";

	// Exception constants
	public static final String PROCESSING_ERROR_FIELD = "ERROR_PROCESSING_PATIENT_RECORD";

	public static final String PROCESSING_ERROR = "Something went wrong internally while processing records. Please try later";

	public static final String PATIENT_ALREADY_EXISTS = "Patient record with given Email Id already exists";

}
