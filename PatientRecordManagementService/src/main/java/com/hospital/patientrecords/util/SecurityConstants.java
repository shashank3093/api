package com.hospital.patientrecords.util;

import org.springframework.context.annotation.Configuration;

@Configuration
public class SecurityConstants {

	public static final String JWT_KEY = "jwt.secret";

	public static final String SIGN_UP_URL = "/users/record";

	public static final String HOSPITAL_SERVICES = "/hospital/**";

	public static final String LOGIN_UP_URL = "/users/login";

	public static final String HEADER_NAME = "Authorization";

	public static final Long EXPIRATION_TIME = 1000L * 60 * 30;

}
