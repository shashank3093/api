package com.hospital.patientrecords.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "PATIENT_DETAILS")
public class Patient implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PAT_ID")
	private long id;

	@Column(name = "PATIENT_NAME")
	@NotBlank(message = "Name is mandatory")
	private String patientName;

	@Column(name = "AGE")
	@NotNull(message = "Age is mandatory")
	@Min(value = 1, message = "Age is mandatory")
	private int age;

	@Column(name = "CONTACT_NUMBER")
	@Size(max = 10, message = "Contact number should not be greater than 10 digits")
	private String contactNumber;

	@Column(name = "EMAIL_ID", unique = true)
	@Email(message = "Email should be valid")
	private String email;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "patient_id")
	private Set<Appointments> appointments;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "patient_id")
	private List<AddressDetail> address;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the patientName
	 */
	public String getPatientName() {
		return patientName;
	}

	/**
	 * @param patientName the patientName to set
	 */
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the appointments
	 */
	public Set<Appointments> getAppointments() {
		return appointments;
	}

	/**
	 * @param appointments the appointments to set
	 */
	public void setAppointments(Set<Appointments> appointments) {
		this.appointments = appointments;
	}

	/**
	 * @return the address
	 */
	public List<AddressDetail> getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(List<AddressDetail> address) {
		this.address = address;
	}

}
