package com.hospital.patientrecords.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "PATIENT_APPOINTMENTS")
public class Appointments implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "APPT_ID")
	private long id;

	@Column(name = "APPT_TYPE")
	private String appointment_Type;

	@Column(name = "APPT_DESC")
	private String appointment_Description;

	@Column(name = "APPT_DT")
	private String appointment__Date;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the appointment_Type
	 */
	public String getAppointment_Type() {
		return appointment_Type;
	}

	/**
	 * @param appointment_Type the appointment_Type to set
	 */
	public void setAppointment_Type(String appointment_Type) {
		this.appointment_Type = appointment_Type;
	}

	/**
	 * @return the appointment_Description
	 */
	public String getAppointment_Description() {
		return appointment_Description;
	}

	/**
	 * @param appointment_Description the appointment_Description to set
	 */
	public void setAppointment_Description(String appointment_Description) {
		this.appointment_Description = appointment_Description;
	}

	/**
	 * @return the appointment__Date
	 */
	public String getAppointment__Date() {
		return appointment__Date;
	}

	/**
	 * @param appointment__Date the appointment__Date to set
	 */
	public void setAppointment__Date(String appointment__Date) {
		this.appointment__Date = appointment__Date;
	}

}
