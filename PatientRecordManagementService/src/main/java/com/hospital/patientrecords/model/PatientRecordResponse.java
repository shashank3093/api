package com.hospital.patientrecords.model;

import java.util.List;

public class PatientRecordResponse {

	private List<Patient> patientRecords = null;

	private List<ValidationError> validationErrors = null;

	/**
	 * @return the patientRecords
	 */
	public List<Patient> getPatientRecords() {
		return patientRecords;
	}

	/**
	 * @param patientRecords the patientRecords to set
	 */
	public void setPatientRecords(List<Patient> patientRecords) {
		this.patientRecords = patientRecords;
	}

	/**
	 * @return the validationErrors
	 */
	public List<ValidationError> getValidationErrors() {
		return validationErrors;
	}

	/**
	 * @param validationErrors the validationErrors to set
	 */
	public void setValidationErrors(List<ValidationError> validationErrors) {
		this.validationErrors = validationErrors;
	}

}
