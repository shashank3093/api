package com.hospital.patientrecords.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hospital.patientrecords.model.Appointments;

public interface AppointmentsRepository extends JpaRepository<Appointments, Long> {

}
