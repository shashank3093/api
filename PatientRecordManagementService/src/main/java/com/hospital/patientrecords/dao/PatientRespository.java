package com.hospital.patientrecords.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hospital.patientrecords.model.Patient;

public interface PatientRespository extends JpaRepository<Patient, Long> {

	@Query("SELECT t FROM PATIENT_DETAILS t where t.patientName = :patientName")
	public Optional<Patient> findPatientByName(@Param("patientName") String patientName);

	@Query("SELECT t FROM PATIENT_DETAILS t where t.email = :email")
	public Optional<Patient> findPatientByEmail(@Param("email") String email);

}
