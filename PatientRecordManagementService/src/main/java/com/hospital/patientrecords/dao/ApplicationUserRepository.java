package com.hospital.patientrecords.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hospital.patientrecords.model.ApplicationUser;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {

	ApplicationUser findByUsername(String username);
}
