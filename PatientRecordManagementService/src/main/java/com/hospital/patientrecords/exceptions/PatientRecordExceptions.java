package com.hospital.patientrecords.exceptions;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.hospital.patientrecords.model.PatientRecordResponse;
import com.hospital.patientrecords.model.ValidationError;
import com.hospital.patientrecords.util.PatientConstants;

@ControllerAdvice
public class PatientRecordExceptions {

	Logger LOG = LoggerFactory.getLogger(PatientRecordExceptions.class);

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<PatientRecordResponse> handleValidationExceptions(MethodArgumentNotValidException ex) {
		PatientRecordResponse patientdetails = new PatientRecordResponse();
		List<ValidationError> validationErrors = new ArrayList<ValidationError>();

		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			createValidationError(validationErrors, fieldName, errorMessage);
		});

		patientdetails.setValidationErrors(validationErrors);
		return new ResponseEntity<>(patientdetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<PatientRecordResponse> handleAllExceptions(Exception ex, WebRequest request) {
		PatientRecordResponse patientdetails = new PatientRecordResponse();
		List<ValidationError> validationErrors = new ArrayList<ValidationError>();
		LOG.info("Error occured " + ex.getMessage());
		LOG.info("Error occured " + ex.getStackTrace());
		createValidationError(validationErrors, PatientConstants.PROCESSING_ERROR_FIELD,
				PatientConstants.PROCESSING_ERROR);

		patientdetails.setValidationErrors(validationErrors);
		return new ResponseEntity<>(patientdetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(PatientAlreadyExistsException.class)
	public final ResponseEntity<PatientRecordResponse> handlePatientExistsExceptions(Exception ex, WebRequest request) {
		PatientRecordResponse patientdetails = new PatientRecordResponse();
		List<ValidationError> validationErrors = new ArrayList<ValidationError>();
		createValidationError(validationErrors, PatientConstants.PROCESSING_ERROR_FIELD, ex.getMessage());
		patientdetails.setValidationErrors(validationErrors);
		return new ResponseEntity<>(patientdetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(PatientNotFoundException.class)
	public final ResponseEntity<PatientRecordResponse> handlePatientNotFoundExceptions(Exception ex,
			WebRequest request) {
		PatientRecordResponse patientdetails = new PatientRecordResponse();
		List<ValidationError> validationErrors = new ArrayList<ValidationError>();
		createValidationError(validationErrors, PatientConstants.M_PATIENT_RECORD, ex.getMessage());
		patientdetails.setValidationErrors(validationErrors);
		return new ResponseEntity<>(patientdetails, HttpStatus.BAD_REQUEST);
	}

	/**
	 * This method creates validation error details
	 * 
	 * @param validationErrors
	 * @param fieldName
	 * @param errorMessage
	 */
	private void createValidationError(List<ValidationError> validationErrors, String fieldName, String errorMessage) {
		ValidationError validationError = new ValidationError();
		validationError.setCode(fieldName);
		validationError.setMessage(errorMessage);
		validationErrors.add(validationError);
	}
}
