package com.hospital.patientrecords;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatientRecordManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(PatientRecordManagementApplication.class, args);
	}

}
