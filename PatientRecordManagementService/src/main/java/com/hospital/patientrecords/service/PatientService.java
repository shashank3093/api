package com.hospital.patientrecords.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.patientrecords.dao.AppointmentsRepository;
import com.hospital.patientrecords.dao.PatientRespository;
import com.hospital.patientrecords.exceptions.PatientAlreadyExistsException;
import com.hospital.patientrecords.exceptions.PatientNotFoundException;
import com.hospital.patientrecords.model.Patient;
import com.hospital.patientrecords.util.PatientConstants;

@Service
public class PatientService {

	@Autowired
	PatientRespository patientRespository;

	@Autowired
	AppointmentsRepository appointmentsRepository;

	/**
	 * This method retrieves the Patient records for the given Patient Name
	 * 
	 * @param patientName
	 * @return Optional<Patient> record
	 */
	public Optional<Patient> getPatientRecords(Patient patientData) {

		Optional<Patient> patientRecord = patientRespository.findById(patientData.getId());

		if (!patientRecord.isPresent()) {
			throw new PatientNotFoundException(PatientConstants.I_PATIENT_RECORD_MSG);
		}
		return patientRecord;
	}

	/**
	 * This method retrieves all Patient records in our system
	 * 
	 * @return List of all Patients
	 */
	public List<Patient> getAllPatients() {
		List<Patient> patientDetails = patientRespository.findAll();
		if (patientDetails.isEmpty()) {
			throw new PatientNotFoundException(PatientConstants.M_PATIENT_RECORD_MSG);
		}
		return patientDetails;
	}

	/**
	 * This method persists the given Patient record in our system
	 * 
	 * @param patient
	 * @return the persisted Patient record
	 */
	public Patient addPatientRecord(Patient patient) {

		if (patientRespository.findPatientByEmail(patient.getEmail()).isPresent()) {
			throw new PatientAlreadyExistsException(PatientConstants.PATIENT_ALREADY_EXISTS);
		}

		return patientRespository.save(patient);
	}

	/**
	 * This method updates the given Patient record in our system
	 * 
	 * @param patient
	 * @return
	 */
	public Patient updatePatientRecord(Patient patient) {

		// Check if patient record exists
		Optional<Patient> patientData = patientRespository.findPatientByEmail(patient.getEmail());

		if (!patientData.isPresent()) {
			throw new PatientNotFoundException(PatientConstants.I_PATIENT_RECORD_MSG);
		}

		patientData = updatePatientDetail(patientData, patient);

		return patientRespository.save(patientData.get());
	}

	/**
	 * This method deletes the given Patient record in our system
	 * 
	 * @param patient
	 * @return
	 */
	public void deletePatientRecord(Patient patient) {
		try {
			patientRespository.deleteById(patient.getId());
		} catch (Exception e) {
			throw new PatientNotFoundException(PatientConstants.I_PATIENT_RECORD_MSG);
		}
	}

	/**
	 * This method helps to update the patient records
	 * 
	 * @param patientData
	 * @param patient
	 * @return
	 */
	private Optional<Patient> updatePatientDetail(Optional<Patient> patientData, Patient patient) {
		patientData.get().setContactNumber(patient.getContactNumber());
		patientData.get().setAddress(patient.getAddress());

		return patientData;

	}

}
