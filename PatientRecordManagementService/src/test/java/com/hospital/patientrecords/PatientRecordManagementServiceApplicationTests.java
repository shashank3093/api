package com.hospital.patientrecords;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import com.hospital.patientrecords.model.Patient;
import com.hospital.patientrecords.model.PatientRecordResponse;

@SpringBootTest(classes = PatientRecordManagementApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class PatientRecordManagementServiceApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testAddPatient() throws Exception {
		Patient patient = new Patient();
		patient.setEmail("asds@asd.com");
		patient.setAge(20);
		patient.setContactNumber("58494984");
		patient.setPatientName("test");
		ResponseEntity<PatientRecordResponse> postResponse = restTemplate
				.postForEntity(getRootUrl() + "/hospital/addPatient", patient, PatientRecordResponse.class);
		assertNotNull(postResponse);
		assertNotNull(postResponse.getBody().getPatientRecords().get(0).getId());
		assertEquals("asds@asd.com", postResponse.getBody().getPatientRecords().get(0).getEmail());
	}

	@Test
	public void testRetrievePatient() throws Exception {
		testAddPatient();
		PatientRecordResponse getResponse = restTemplate.getForObject(getRootUrl() + "/hospital/patients/1",
				PatientRecordResponse.class);
		assertNotNull(getResponse);
		assertNotNull(getResponse.getPatientRecords().get(0).getId());
		assertEquals("asds@asd.com", getResponse.getPatientRecords().get(0).getEmail());
	}

	@Test
	public void testUpdatePatient() throws Exception {
		testAddPatient();
		PatientRecordResponse getResponse = restTemplate.getForObject(getRootUrl() + "/hospital/patients/1",
				PatientRecordResponse.class);
		Patient patient = getResponse.getPatientRecords().get(0);
		patient.setContactNumber("456466");

		restTemplate.put(getRootUrl() + "/hospital/updatePatient", patient);

		PatientRecordResponse getUpdatedResponse = restTemplate.getForObject(getRootUrl() + "/hospital/patients/1",
				PatientRecordResponse.class);

		assertNotNull(getUpdatedResponse);
		assertEquals("456466", getUpdatedResponse.getPatientRecords().get(0).getContactNumber());
	}

	@Test
	public void testDeletePatient() throws Exception {
		testAddPatient();
		PatientRecordResponse getResponse = restTemplate.getForObject(getRootUrl() + "/hospital/patients/1",
				PatientRecordResponse.class);
		assertNotNull(getResponse);
		restTemplate.delete(getRootUrl() + "/hospital/deletePatient/1");
		try {
			restTemplate.getForObject(getRootUrl() + "/hospital/patients/1", PatientRecordResponse.class);
		} catch (final HttpClientErrorException e) {
			assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}

}
